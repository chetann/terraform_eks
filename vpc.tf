provider "aws" {
  region = "ap-south-1"
}

variable vpc_cidr_block {  }
variable private_subnet_codr_blocks {  }
variable public_subnet_codr_blocks {  }

data "aws_availability_zones" "azs" {
  
}


module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "myapp-vpc"
  cidr =  var.vpc_cidr_block 
  private_subnets = var.private_subnet_codr_blocks
  public_subnets = var.public_subnet_codr_blocks
  azs =   data.aws_availability_zones.azs.names

  enable_nat_gateway = true
  enable_dns_hostnames = true
  single_nat_gateway = true

  tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }
  private_subnet_tags = {
      "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
      "kubernetes.io/role/inetrnal-elb" = 1
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}